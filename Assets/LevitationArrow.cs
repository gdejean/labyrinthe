﻿using UnityEngine;
using System.Collections;

public class LevitationArrow : MonoBehaviour
{
	
	public float velocity = 2f;

	void Start(){
		StartCoroutine (switchDirection ());
	}

	void Update(){

		transform.Translate (velocity * Time.deltaTime * Vector3.up,Space.Self);

			
	}

	IEnumerator switchDirection (){
		while (true) {
			yield return new WaitForSeconds(0.6f);
			velocity = -velocity;
		}
	}


}