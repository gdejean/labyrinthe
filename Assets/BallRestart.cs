using UnityEngine;
using System.Collections;

public class BallRestart : MonoBehaviour {

	private Vector3 startPosition;
	public Transform plateau;           // sol

	// Use this for initialization
	void Start () {
		plateau.rotation = Quaternion.Euler(0, 0.0f, 0);
		startPosition = transform.position;	
	}
	
	// Update is called once per frame
	void Respawn () {
		plateau.rotation = Quaternion.Euler(0, 0.0f, 0);
		transform.position = startPosition;

	}
}