﻿using UnityEngine;
using System.Collections;

public class MoveObs : MonoBehaviour
{
	
	public float velocity = 1f;
	public float h = 1f;
	
	void Start(){
		StartCoroutine (switchDirection ());
	}
	
	void Update(){
		transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z + (velocity *Time.deltaTime));
		transform.Translate (h/2 * Time.deltaTime * Vector3.back,Space.Self);
		
		
	}
	
	IEnumerator switchDirection (){
		while (true) {
			yield return new WaitForSeconds(1f);
			velocity = -velocity;
			h = -h;
		}
	}
	
	
}