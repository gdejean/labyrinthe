
var target : Transform;

//Distance x-z plan
var distance = 10.0;
// Hauteur 
var height = 5.0;
// How much we 
var heightDamping = 2.0;
var rotationDamping = 3.0;


@script AddComponentMenu("Camera-Control/Smooth Follow")


function LateUpdate () {

	if (!target)
		return;
	
	//Calcul de l'angle
	wantedRotationAngle = target.eulerAngles.y;
	wantedHeight = target.position.y + height;
		
	currentRotationAngle = transform.eulerAngles.y;
	currentHeight = transform.position.y;
	
	currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);

	currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping * Time.deltaTime);

	currentRotation = Quaternion.Euler (0, currentRotationAngle, 0);
	
	transform.position = target.position;
	transform.position -= currentRotation * Vector3.forward * distance;

	transform.position.y = currentHeight;
	
	transform.LookAt (target);
}