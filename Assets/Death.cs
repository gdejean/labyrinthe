﻿using UnityEngine;
using System.Collections;

public class Death : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter (Collider other){
		if (other.tag.Equals("Player")) {
			if(this.tag.Equals("Kill")){
				gameObject.SendMessage("Restart");

				other.SendMessage("Respawn");

				}
		}
	}
}
