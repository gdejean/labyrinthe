﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour
{
	public float smooth = 1.5f;         // La vitesse de la caméra
	
	
	public Transform player;           // Joueur
	private Vector3 relCameraPos;       // Position de la camera par rapport au joueur
	private float relCameraPosMag;      // La distance de la camera
	private Vector3 newPos;             // La nouvelle position de la caméra
	
	
	void Awake ()
	{
		relCameraPos = transform.position - player.position;
		relCameraPosMag = relCameraPos.magnitude - 0.5f;
	}
	
	
	void FixedUpdate ()
	{
		
		Vector3 standardPos = player.position + relCameraPos;
		Vector3 abovePos = player.position + Vector3.up * relCameraPosMag;
		Vector3[] checkPoints = new Vector3[5];
		
		checkPoints[0] = standardPos;
		checkPoints[1] = Vector3.Lerp(standardPos, abovePos, 0.25f);
		checkPoints[2] = Vector3.Lerp(standardPos, abovePos, 0.5f);
		checkPoints[3] = Vector3.Lerp(standardPos, abovePos, 0.75f);
		checkPoints[4] = abovePos;
		
		for(int i = 0; i < checkPoints.Length; i++)
		{
			if(ViewingPosCheck(checkPoints[i]))
				break;
		}
		
		transform.position = Vector3.Lerp(transform.position, newPos, smooth * Time.deltaTime);
		SmoothLookAt();
	}
	
	
	bool ViewingPosCheck (Vector3 checkPos)
	{
		RaycastHit hit;
		
		if(Physics.Raycast(checkPos, player.position - checkPos, out hit, relCameraPosMag))
			if(hit.transform != player)
				return false;
		
		newPos = checkPos;
		return true;
	}
	
	
	void SmoothLookAt ()
	{
		Vector3 relPlayerPosition = player.position - transform.position;
		Quaternion lookAtRotation = Quaternion.LookRotation(relPlayerPosition, Vector3.up);
		transform.rotation = Quaternion.Lerp(transform.rotation, lookAtRotation, smooth * Time.deltaTime);
	}
}