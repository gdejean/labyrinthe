﻿using UnityEngine;
using System.Collections;

public class EndLevel : MonoBehaviour
{
	
	// Use this for initialization
	void Start ()
	{
		
	}

	void OnTriggerEnter (Collider other){
		if (other.tag == "Player") {
			if(this.tag == "Loose"){
				other.SendMessage("Respawn");
			}else {
				if (Application.loadedLevelName.Equals("easy")){
					other.SendMessage("Medium");
				} 
				else if(Application.loadedLevelName.Equals("medium")){
					other.SendMessage("Hard");
				} else if(Application.loadedLevelName.Equals("hard")){
					other.SendMessage("Movement");
				}else {
					other.SendMessage("Extreme");
				}
			}
		}
	}
	// Update is called once per frame

}