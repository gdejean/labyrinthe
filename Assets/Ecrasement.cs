﻿using UnityEngine;
using System.Collections;

public class Ecrasement : MonoBehaviour {

	bool ecraser; 

	// Use this for initialization
	void Start () {
		ecraser = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter (Collider other){
		if (other.tag.Equals("Crash") || other.tag.Equals("Mur")) {
			if(ecraser){
				this.SendMessage("Respawn");
			}
			else {
				ecraser = true;
			}
		}
	}

	void OnTriggerExit (Collider other){
		if (other.tag.Equals ("Crash") || other.tag.Equals ("Mur")) {
				ecraser = false;
		}
	}
}
