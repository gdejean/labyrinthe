using UnityEngine;
using System.Collections;

public class MyPlaneBehaviourScript : MonoBehaviour 
{
	float m_rotx;
	float m_rotz;
	float m_speed;
	
	// Use this for initialization
	void Start () 
	{
		m_rotx = 0.0f;
		m_rotz = 0.0f;
		m_speed = 20.0f;
	}
	
	// Update is called once per frame
	void Update () 
	{

		bool needRecomputeRotation = false;
		
		if(Input.GetKey("left"))
		{
			if(m_rotz + Time.deltaTime * m_speed < 50 && m_rotz + Time.deltaTime * m_speed > -50 ){

			m_rotz += Time.deltaTime * m_speed;
			needRecomputeRotation = true;
			}
		}
		else if(Input.GetKey("right"))
		{
			if(m_rotz - Time.deltaTime * m_speed < 50 && m_rotz - Time.deltaTime * m_speed > -50 ){

			m_rotz -= Time.deltaTime * m_speed;
			needRecomputeRotation = true;
			}
		}
		
		if(Input.GetKey("up"))
		{
			if(m_rotx + Time.deltaTime * m_speed < 50 && m_rotx + Time.deltaTime * m_speed > -50 ){

			m_rotx += Time.deltaTime * m_speed;
			needRecomputeRotation = true;
			}

		}
		else if(Input.GetKey("down"))
		{
			if(m_rotx - Time.deltaTime * m_speed < 50 && m_rotx - Time.deltaTime * m_speed > -50 ){

			m_rotx -= Time.deltaTime * m_speed;
			needRecomputeRotation = true;
			}
		}
		if(needRecomputeRotation)
		{
			myFunctionComputeRotation();
		}
	}
	
	void myFunctionComputeRotation()
	{
		transform.rotation = Quaternion.Euler(m_rotx, 0.0f, m_rotz);
	
	}

	void Restart () {
		transform.rotation = Quaternion.Euler(0, 0.0f, 0);
		m_rotx = 0;
		m_rotz = 0;

	}
}
